package si.uni_lj.fri.lrk.lab7

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import si.uni_lj.fri.lrk.lab7.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    var mBcastRecv: AccBroadcastReceiver? = null

    // TODO: Uncomment MachineLearningManager declaration
    // var mManager: MachineLearningManager? = null
    var handler: Handler? = null
    var mSensing: Boolean? = null
    var mTraining: Boolean? = null

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        mBcastRecv = AccBroadcastReceiver()
        handler = Handler(Looper.getMainLooper())
        mSensing = false
        mTraining = false
        binding.btnControl.setOnClickListener {
            if (mSensing as Boolean) {
                binding.btnControl.setText(R.string.txt_start)
                stopSensing()
            } else {
                binding.btnControl.setText(R.string.txt_stop)
                startSensing()
            }
        }
        binding.swTraining.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                mTraining = true
                binding.tvSelect.visibility = View.VISIBLE
                binding.radioGroup.visibility = View.VISIBLE
                binding.tvResult.visibility = View.INVISIBLE
            } else {
                mTraining = false
                binding.tvSelect.visibility = View.INVISIBLE
                binding.radioGroup.visibility = View.INVISIBLE
                binding.tvResult.visibility = View.VISIBLE
            }
        }
        initClassifier()
    }

    override fun onStart() {
        super.onStart()

        // TODO: Register local broadcast receiver
    }

    override fun onStop() {
        super.onStop()

        // TODO: Unregister local broadcast receiver
    }

    fun startSensing() {
        Log.d(TAG, "startSensing()")
        mSensing = true

        // TODO: set Handler to run AccSenseService every five seconds
    }

    fun stopSensing() {
        Log.d(TAG, "stopSensing()")
        mSensing = false
        handler?.removeMessages(0)
        binding.container.setBackgroundColor(Color.WHITE)
    }

    fun initClassifier() {
        Log.d(TAG, "initClassifier")

        // TODO: Instantiate the classifier
    }

    fun recordAccData(mean: Float, variance: Float, MCR: Float) {
        Log.d(TAG, "recordAccData Intensity: $mean var $variance MCR $MCR")
        if (binding.swTraining.isChecked) {

            // TODO: get the label of the selected radio button

            // TODO: send data to TrainClassifierService
        } else {

            // TODO: Do the inference (classification) and set the result (also screen background colour)
        }
    }

    inner class AccBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d(TAG, " AccBroadcastReceiver onReceive...")
            val mean = intent.getFloatExtra("mean", 0f)
            val variance = intent.getFloatExtra("variance", 0f)
            val mcr = intent.getFloatExtra("MCR", 0f)
            Log.d(TAG, "recordAccData Intensity: $mean var $variance MCR $mcr")
            recordAccData(mean, variance, mcr)
        }
    }

    companion object {
        const val TAG = "MainActivity"
        const val ACTION_CLASSIFIER_TRAINING = "si.uni_lj.fri.lrk.lab7.TRAIN_CLASSIFIER"
        const val ACTION_SENSING_RESULT = "si.uni_lj.fri.lrk.lab7.SENSING_RESULT"
    }
}