package si.uni_lj.fri.lrk.lab7

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.JobIntentService

class TrainClassifierService : JobIntentService() {
    override fun onHandleWork(intent: Intent) {
        Log.d(TAG, "onHandleIntent")
        if (intent.action == MainActivity.ACTION_CLASSIFIER_TRAINING) {
            val accMean = intent.getFloatExtra("accMean", 0f)
            val accVar = intent.getFloatExtra("accVar", 0f)
            val accMCR = intent.getFloatExtra("accMCR", 0f)
            val label = intent.getStringExtra("label")
            trainClassifier(accMean, accVar, accMCR, label.toString())
        }
    }

    fun trainClassifier(mean: Float, accVar: Float, MCR: Float, label: String) {
        Log.d(TAG, "trainClassifier with $mean $accVar $MCR $label")

        // TODO: Train the classifier here; include mean, variance, and MCR
    }

    companion object {
        private const val TAG = "TrainClassifierService"
        private val JOB_ID = 2
        fun enqueueWork(context: Context?, intent: Intent?) {
            enqueueWork(context!!, TrainClassifierService::class.java, JOB_ID, intent!!)
        }
    }
}