package si.uni_lj.fri.lrk.lab7

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.Log
import androidx.core.app.JobIntentService
import java.util.*

class AccSenseService : JobIntentService(), SensorEventListener {
    private var mSensorManager: SensorManager? = null
    private var mSensorReadings: ArrayList<FloatArray>? = null


    override fun onCreate() {
        super.onCreate()
        mSensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        Log.d(TAG, "onCreate")
    }


    override fun onHandleWork(intent: Intent) {
        Log.d(TAG, "onHandleWork")
        if (mSensorManager?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            mSensorManager!!.registerListener(this,
                    mSensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    SensorManager.SENSOR_DELAY_NORMAL)
            mSensorReadings = ArrayList()
        }

    }

    private fun getAccelerometer(event: SensorEvent) {
        val values = event.values
        val x = values[0]
        val y = values[1]
        val z = values[2]
        mSensorReadings!!.add(floatArrayOf(x, y, z))
    }

    override fun onSensorChanged(event: SensorEvent) {
        Log.d(TAG, "onSensorChanged")
        getAccelerometer(event)
        if (mSensorReadings!!.size > 49) {

            // If there are 50 samples, we calculate mean, variance and mean crossing rate
            mSensorManager!!.unregisterListener(this)
            var mean = 0f
            var variance = 0f
            var mcr = 0f
            var totalIntensity = 0f
            val intensityList = FloatArray(mSensorReadings!!.size)
            for (i in mSensorReadings!!.indices) {
                val sample = mSensorReadings!![i]
                val intensity = Math.sqrt(Math.pow(sample[0].toDouble(), 2.0) + Math.pow(sample[1].toDouble(), 2.0) + Math.pow(sample[2].toDouble(), 2.0)).toFloat()
                intensityList[i] = intensity
                totalIntensity += intensity
            }
            if (mSensorReadings!!.size > 0) {
                mean = totalIntensity / mSensorReadings!!.size
                for (i in mSensorReadings!!.indices) {
                    variance += Math.pow((intensityList[i] - mean).toDouble(), 2.0).toFloat()
                    Log.d(TAG, "var one: " + Math.pow((intensityList[i] - mean).toDouble(), 2.0) + " intensityList[i] " + intensityList[i] + " mean: " + mean)
                    if (i > 0) {
                        if ((intensityList[i] - mean) * (intensityList[i - 1] - mean) < 0) {
                            mcr++
                        }
                    }
                }
                variance /= mSensorReadings!!.size
                if (mSensorReadings!!.size > 1) {
                    mcr /= (mSensorReadings!!.size - 1)
                }
            }

            // TODO: Broadcast the results (mean, variance, and MCR) to MainActivity by using
            //  a local broadcast with ACTION_SENSING_RESULT action
            Log.d(TAG, "$mean, $variance, $mcr")
        }
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
    override fun onDestroy() {
        super.onDestroy()
    }

    companion object {
        private val JOB_ID = 2
        const val TAG = "AccSenseService"
        fun enqueueWork(context: Context?, intent: Intent?) {
            enqueueWork(context!!, AccSenseService::class.java, JOB_ID, intent!!)
        }
    }

    init {
        Log.d(TAG, "AccSenseService")
    }
}